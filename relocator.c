#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>

//That is the actual path length limitation.
static int buffer_size = 1000;

//Chech that the "whitelist_size" variable corresponds to the real array size!
int whitelist_size = 11;
const char *whitelist_array[] = {
    "/bin/",
    "/etc/",
    "/lib/",
    "/lib32/",
    "/lib64/",
    "/libx32/",
    "/opt/",
    "/sbin/",
    "/snap/",
    "/srv/",
    "/usr/"
};

static int (*real_access)(const char* name, int type) = NULL;
static int (*real_openat)(int fd, const char* file, int oflag, ...) = NULL;
static int (*real_statx)(int dirfd, const char* restrict path, int flags, unsigned int mask, struct statx* restrict buf) = NULL;
static int (*real_stat)(const char* restrict file, struct stat* restrict buf) = NULL;
static ssize_t (*real_readlink)(const char* restrict path, char* restrict buf, size_t len) = NULL;

const char* try_relocating(const char* path)
{
    const char* relocator_path = getenv("RELOCATOR_PATH");
    char* fake_path = calloc(buffer_size, 1);
    int whitelist = 0;
    int file_present = 0;
    
    for (int cnt = 0; cnt < whitelist_size; ++cnt)
    {
        if(strstr(path, whitelist_array[cnt]))
        {
            whitelist = 1;
        }
    }

    strcpy(fake_path, relocator_path);
    strcat(fake_path, path);

    if (fileabsent(fake_path) == 0)
    {
        file_present = 1;
    }
    
    if (strlen(path) > 1 && whitelist && file_present)
    {
        return(fake_path);
    }
    else
    {
        return(path);
    }
}

int access(const char* name, int type)
{
    real_access = dlsym(RTLD_NEXT, "access");
    real_access(try_relocating(name), type);
}
int openat(int fd, const char* file, int oflag, ...)
{
    real_openat = dlsym(RTLD_NEXT, "openat");
    real_openat(fd, try_relocating(file), oflag);
}
int statx(int dirfd, const char* restrict path, int flags, unsigned int mask, struct statx* restrict buf)
{
    real_statx = dlsym(RTLD_NEXT, "statx");
    real_statx(dirfd, try_relocating(path), flags, mask, buf);
}
int stat(const char* restrict file, struct stat* restrict buf)
{
    real_stat = dlsym(RTLD_NEXT, "stat");
    real_stat(try_relocating(file), buf);
}
ssize_t readlink(const char* restrict path, char* restrict buf, size_t len)
{
    real_readlink = dlsym(RTLD_NEXT, "readlink");
    real_readlink(try_relocating(path), buf, len);
}
