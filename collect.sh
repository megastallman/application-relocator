#!/bin/bash

REAL_FS_LIST="etc lib lib64 usr bin sbin"

if [ -z "$1" ]
then
    echo "ERROR: No binary to trace"
    echo "Usage: ./collect.sh ktechlab"
    exit 1
fi

function copy_file {
    FULLPATH=${1}
    CURRENTDIR=$PWD
    FILENAME=$(basename ${FULLPATH})
    DIRNAME=$(echo ${FULLPATH:1} | sed "s/${FILENAME}//")
    echo "Processing file: $FULLPATH"
    mkdir -p $DIRNAME || true
    cp $FULLPATH ${CURRENTDIR}${FULLPATH} || true
}

strace ${1} 2> strace.log

echo "================\\"
echo "Tracing finished |"
echo "================/"

LIST1=$(cat strace.log | sed 's/"/\n/g' | grep '^/' | sort | uniq)

for i in $REAL_FS_LIST
do
    ii="/${i}"
    for j in $LIST1
    do
        if [[ $j == "$ii"* ]]
        then
            copy_file "$j"
        fi
    done
done
